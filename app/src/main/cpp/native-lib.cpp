#include <jni.h>
#include <string>

extern "C" {
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libavutil/display.h>
}


extern "C" JNIEXPORT jstring JNICALL
Java_com_test_mFFmpeg_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {

    AVFormatContext *pFormatCtx;

    //avcodec_version avformat_version
    avformat_version();
    //avcodec_parameters_alloc();
    std::string hello = "Hello from C++ test " + std::to_string(avformat_version());
    return env->NewStringUTF(hello.c_str());
}