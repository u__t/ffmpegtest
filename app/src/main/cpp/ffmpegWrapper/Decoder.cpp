//
// Created by tyan_ on 05.05.2019.
//


#include "Decoder.h"
#include "ffmpegLayer.h"
#include <android/log.h>
#include <string>


extern "C" {
#include <libavutil/frame.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}
Decoder::Decoder() {}

bool Decoder::decode(const Video_unit & src) {

    if (!reinit(src)) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] failed to reinit");
        return false;
    }

    ffmpegLayer::prepare_input_buffer(*src.data_ptr);

    AVPacket pkt = AVPacket();

    av_init_packet(&pkt);

    pkt.data = src.data_ptr->data() + src.pkt_data_offset;//src.buf;//buf;
    pkt.size = static_cast<int>(src.data_ptr->size()) - src.pkt_data_offset;//src.size;//len;



    size_t frames = 0;
    while(pkt.data && (pkt.size > 0))
    {
        int got_frame = 0;
        const int r = avcodec_decode_video2(_codec_context.get(), _frame.get(), &got_frame, &pkt);

        if(r < 0){
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] some error");
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(r).c_str());
            break;
            //return false;
        }

        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] test");
        if(got_frame){
            ++frames;
        }

        pkt.size -= r;
        pkt.data = (pkt.size > 0) ? (pkt.data + r) : nullptr;

        //workaround
        if( frames)
        {
            break;
        }
    }
    //const int r = avcodec_decode_video2(_codec_context.get(), _frame.get(), &got_frame, &pkt);

    av_packet_unref(&pkt);
    if (!ffmpegLayer::is_valid_frame(*_frame)) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] invalid frame detected, reinit");
        close();
        reinit(src);
        return false;
    }

    return true;
}


std::shared_ptr<AVFrame> Decoder::getFrame()
{
    return _frame;
}

bool Decoder::reinit(const Video_unit & src) {

    if(_codec_context && (_codec_context->codec_id == src.codec))
    {
        return true;
    }
    close();
    __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] reinit");
    AVCodec * avcodec = avcodec_find_decoder(src.codec);
    if(!avcodec) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] failed to find av decoder for");
        return false;
    }

    AVCodecContext * avcontext = avcodec_alloc_context3(avcodec);
    if(!avcontext) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] failed to alloc codec context");
        return false;
    }

    _codec_context.reset(avcontext, &ffmpegLayer::avCodecFreeContext);

    _codec_context->thread_type = 0;

    const int r = avcodec_open2(avcontext, avcodec, nullptr);
    if(r < 0) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] failed to open codec");
        close();
        return false;
    }
    _frame.reset(av_frame_alloc(), &ffmpegLayer::av_frame_free);
    if(!_frame) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_decoder] failed to alloc frame");
        close();
        return false;
    }

    return true;
}

void Decoder::close()
{
    _frame.reset();
    _codec_context.reset();
}
