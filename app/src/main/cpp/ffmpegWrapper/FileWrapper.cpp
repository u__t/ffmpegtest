//
// Created by tyan_ on 16.06.2019.
//

#include <vector>
#include "FileWrapper.h"

void FileWrapper::saveData(const void*buf, size_t len, std::string path, std::string name) {
    std::string fullName = path + name;
    FILE* file = fopen(fullName.c_str(),"w+");

    if (file != NULL)
    {
        fwrite(buf, 1, len, file );
        fflush(file);
        fclose(file);
    }
}