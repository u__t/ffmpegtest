//
// Created by tyan_ on 16.05.2019.
//

#include <android/log.h>
#include <string>
#include "Scaler.h"

bool Scaler::scale(ImageFrame &dst, const AVFrame &src) {
    const int width = dst.width;
    const int height = dst.height;

    __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(src.format).c_str());

    int format = src.format == AV_PIX_FMT_YUVJ420P ? AV_PIX_FMT_YUV420P :
                 src.format == AV_PIX_FMT_YUVJ422P ? AV_PIX_FMT_YUV422P : src.format;

    //format = AV_PIX_FMT_RGB24;

    __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(format).c_str());

    if (format < 0) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_scaler] bad frame format");
        return false;
    }

    _scaler_context = sws_getCachedContext(_scaler_context,
                                           src.width,
                                           src.height,
                                           static_cast<AVPixelFormat>(format),
                                           width,
                                           height,
                                           AV_PIX_FMT_RGB24,
                                           0, nullptr, nullptr, nullptr);

    if (!_scaler_context) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_scaler] failed to init scaler context");
        return false;
    }

    dst.pitch = (width + /*AV_INPUT_BUFFER_PADDING_SIZE*/32) * 3;
    dst.data.resize(static_cast<size_t>(dst.height * dst.pitch));
    //uint8_t* data = new uint8_t[static_cast<size_t>(height * dst.pitch)];

    alignas(64) uint8_t * const src_data[4] = { src.data[0], src.data[1], src.data[2], 0 };
    alignas(64) const int src_size[4] = { src.linesize[0], src.linesize[1], src.linesize[2], 0 };

    alignas(64) uint8_t * const dst_data[1] = { dst.data.data() };
    alignas(64) const int dst_size[1] = { dst.pitch };

    const int r = sws_scale(_scaler_context, src_data, src_size, 0, src.height, dst_data, dst_size);

    if(r != height)
    {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "[video_scaler] failed to scale frame: ");
        return false;
    }
    //dst.buf = data;
    //size_t rSize = static_cast<size_t>(height * pitch);
    //dst.size = static_cast<int>(rSize);

    return true;
}