//
// Created by tyan_ on 16.06.2019.
//

#ifndef TESTPROJECT2_FILEWRAPPER_H
#define TESTPROJECT2_FILEWRAPPER_H

#include <string>

class FileWrapper {
public:
    void saveData(const void*buf, size_t len, std::string path, std::string name);
    void readFile(std::string path);
};


#endif //TESTPROJECT2_FILEWRAPPER_H
