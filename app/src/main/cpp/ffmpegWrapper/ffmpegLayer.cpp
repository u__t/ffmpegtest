//
// Created by tyan_ on 09.05.2019.
//

#include <android/log.h>
#include <string>
#include "ffmpegLayer.h"

extern "C" {
#include <libavcodec/avcodec.h>
}

namespace ffmpegLayer
{
    void avCodecFreeContext(AVCodecContext * context)
    {
        avcodec_free_context(&context);
    }

    void av_frame_free(AVFrame * frame)
    {
        av_frame_free(&frame);
    }

    bool is_valid_frame_row(const uint8_t * data, int size)
    {
        const uint8_t first = *data;

        if(first < 48) //may be real dark picture
        {
            return true;
        }

        //todo <media> optimize
        for(int i = 1; i < size; ++i)
        {
            if(data[i] != first)
            {
                return true;
            }
        }

        return false;
    }

    bool is_valid_frame(const AVFrame & frame)
    {
        const uint8_t * data = frame.data[0];
        const int width = frame.width;
        const int height = frame.height;
        const int pitch = frame.linesize[0];

        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "--is_valid_frame--");
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(width).c_str());
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(height).c_str());
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(pitch).c_str());
        //__android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(is_valid_frame_row(data + pitch * height / 4, width)).c_str());
        //__android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(is_valid_frame_row(data + pitch * height / 2, width)).c_str());

        return data && width && height && pitch &&
               (is_valid_frame_row(data + pitch * height / 4, width) || is_valid_frame_row(data + pitch * height / 2, width));
    }

    void prepare_input_buffer(std::vector<uint8_t> & data)
    {
        const size_t size = data.size();

        data.resize(data.size() + AV_INPUT_BUFFER_PADDING_SIZE);
        memset(&data[size], 0, AV_INPUT_BUFFER_PADDING_SIZE);
        data.resize(size);

    }
}