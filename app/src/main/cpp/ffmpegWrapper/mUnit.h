//
// Created by tyan_ on 10.05.2019.
//

#ifndef TESTPROJECT2_MUNIT_H
#define TESTPROJECT2_MUNIT_H

#include <vector>
#include <cstdint>
extern "C" {
#include <libavcodec/avcodec.h>
}

struct Video_frame
{
    struct Plane
    {
        std::vector<uint8_t> data;

        int width;
        int height;
        int pitch;

        Plane()
                : width(0)
                , height(0)
                , pitch(0)
        {}
    };//Plane

    //QDateTime timestamp;

    Plane y;
    Plane u;
    Plane v;

    Video_frame()
    {}
};//Video_frame

struct ImageFrame
{
    int width;
    int height;
    int pitch;
    std::vector<uint8_t> data;

    ImageFrame()
            : width(0)
            , height(0)
            , pitch(0)
    {}

    ImageFrame(int w, int h)
            : width(w)
            , height(h)
            , pitch(0)
    {}

};//ImageFrame

struct mUnit {
    int size;
    //uint8_t* buf;
    std::shared_ptr<std::vector<uint8_t>> data_ptr;
    int pkt_data_offset;
    mUnit():size(0),pkt_data_offset(0){}
};


struct Video_unit
{
    typedef AVCodecID codec_t;

    //QDateTime timestamp;
    codec_t codec;
    int gop_index;
    int width;
    int height;
    std::shared_ptr<std::vector<uint8_t>> data_ptr;
    int pkt_data_offset;

    Video_unit()
            : //timestamp()
             codec(AV_CODEC_ID_NONE)
            , gop_index(0)
            , width(0)
            , height(0)
            , pkt_data_offset(0)
    {}
};//Video_unit

//typedef std::shared_ptr<Video_unit> video_unit_ptr_t;
//typedef std::vector<video_unit_ptr_t> video_units_t;

//AVCodecID net2_codec_id(const QString& codec_name);

#endif //TESTPROJECT2_MUNIT_H
