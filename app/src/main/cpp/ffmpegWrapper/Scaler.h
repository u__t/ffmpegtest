//
// Created by tyan_ on 16.05.2019.
//

#ifndef TESTPROJECT2_SCALER_H
#define TESTPROJECT2_SCALER_H
#include "mUnit.h"
extern "C" {
#include <libavutil/frame.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

class Scaler {
public:
    bool scale(ImageFrame &dst, const AVFrame & src);

private:
    SwsContext * _scaler_context;
};


#endif //TESTPROJECT2_SCALER_H
