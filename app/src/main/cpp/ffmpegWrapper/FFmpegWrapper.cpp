#include <jni.h>
#include <string>
#include <android/log.h>

#include <iostream>
#include <fstream>
#include <android/bitmap.h>"
extern "C" {
#include <libavformat/avformat.h>
}

#include "Decoder.h"
#include "Scaler.h"
#include "FileWrapper.h"


static Scaler scaler;
static Decoder decoder;
extern "C" JNIEXPORT jobjectArray JNICALL
Java_com_test_mFFmpeg_FFmpegWrapper_testByteArray3( JNIEnv* env, jobject thiz, jobjectArray data, jobjectArray names, jint width, jint height, jint codecId) {

    Video_unit unit1 = Video_unit();
    unit1.width = width;
    unit1.height = height;
    switch (codecId) {
        case 0:
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-AV_CODEC_ID_H264-");
            unit1.codec = AV_CODEC_ID_H264;
            break;
        case 1:
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-AV_CODEC_ID_MPEG4-");
            unit1.codec = AV_CODEC_ID_MPEG4;
            break;
        case 2:
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-AV_CODEC_ID_MJPEG-");
            unit1.codec = AV_CODEC_ID_MJPEG;
            break;
        default:
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-AV_CODEC_ID_MJPEG-");
            unit1.codec = AV_CODEC_ID_MJPEG;
    }


    int len = env->GetArrayLength (data);
    __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(len).c_str());


    //----------------------------------------------------------------------------------------------
    jbyteArray arrTest = (jbyteArray) env->GetObjectArrayElement(data, 0);
    jclass cls_arraylist = env->GetObjectClass(arrTest);
    jobjectArray outJNIArray = env->NewObjectArray(len, cls_arraylist, 0);
    //----------------------------------------------------------------------------------------------

    for(int i = 0; i < len; i++) {
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-start-");
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(i).c_str());

        jstring string = (jstring) (env->GetObjectArrayElement(names, i));
        const char *nameStr = env->GetStringUTFChars(string, 0);//read name

        jbyteArray arr = (jbyteArray) env->GetObjectArrayElement(data, i);
        jsize innerLen = env->GetArrayLength(arr);
        __android_log_write(ANDROID_LOG_ERROR, "jni_tag", std::to_string(innerLen).c_str());

        unit1.data_ptr = std::make_shared<std::vector<uint8_t>>(innerLen + 32, 0);
        unit1.data_ptr->resize(innerLen);
        env->GetByteArrayRegion (arr, 0, innerLen, reinterpret_cast<jbyte*>(unit1.data_ptr->data()));

        bool decodeResult = decoder.decode(unit1);

        if(decodeResult) {
            __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-decode ok-");

            ImageFrame sResult(unit1.width, unit1.height);
            bool res = scaler.scale(sResult, *decoder.getFrame().get());

            if(res) {
                __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-ok-");
            } else {
                __android_log_write(ANDROID_LOG_ERROR, "jni_tag", "-fail-");
            }

            //конвертация результата в jbyteArray
            jbyteArray result = env->NewByteArray( sResult.data.size());
            //__android_log_write(ANDROID_LOG_ERROR, "jni_tag", sResult.data.size());
            env->SetByteArrayRegion(result, 0, sResult.data.size(), reinterpret_cast<jbyte*>(sResult.data.data()));

            env->SetObjectArrayElement(outJNIArray,i,result);
        }
    }

    return outJNIArray;
}
