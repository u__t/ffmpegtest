//
// Created by tyan_ on 05.05.2019.
//

#ifndef TESTPROJECT2_DECODER_H
#define TESTPROJECT2_DECODER_H

#include <memory>
#include <string>
#include "mUnit.h"

extern "C" {
#include <libavutil/frame.h>
#include <libavcodec/avcodec.h>
}

class Decoder {
public:
    Decoder();

    bool decode(const Video_unit & src);
    std::shared_ptr<AVFrame> getFrame();

private:
    std::shared_ptr<AVCodecContext> _codec_context;
    std::shared_ptr<AVFrame> _frame;

    bool reinit(const Video_unit & src);
    void close();
    //void SaveFrame(AVFrame *pFrame, int width, int height, int iFrame);
    //int frameToJpeg(AVCodecContext *pCodecCtx, AVFrame *pFrame, AVPacket *result);
    //bool openFile(std::string filePath);

};


#endif //TESTPROJECT2_DECODER_H
