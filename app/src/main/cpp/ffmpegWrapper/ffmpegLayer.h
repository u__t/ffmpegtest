//
// Created by tyan_ on 09.05.2019.
//

#ifndef TESTPROJECT2_FFMPEGLAYER_H
#define TESTPROJECT2_FFMPEGLAYER_H

#include <vector>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/frame.h>
}

namespace ffmpegLayer
{
    void avCodecFreeContext(AVCodecContext * context);
    void av_frame_free(AVFrame * frame);
    bool is_valid_frame(const AVFrame & frame);
    void prepare_input_buffer(std::vector<uint8_t> & data);
}


#endif //TESTPROJECT2_FFMPEGLAYER_H
