package com.test.mFFmpeg;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    static private final String TAG = "mTag";
    static final int PICKFILE_RESULT_CODE = 1;
    static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    //private VideoView videoView;
    private Button openBtn;
    private Button startBtn;
    private ImageView imageView;

    private FFmpegWrapper wrapper;
    private int selectedCodec = 0;
    private String selectedPath = "";
    //private String  selectedName = "";

    private int currentImage = 0;

    Handler mIncomingHandler = new Handler((msg) -> {
        Bundle bundle = msg.getData();
        String date = bundle.getString("Key");
        if(null != date) {
            setText(date);
            if(!date.equals("декодирование...")) {
                stopProgress();
            }
        }
        return true;
    });

    Handler mImageHandler = new Handler((msg) -> {
        Bundle bundle = msg.getData();
        Bitmap bitmap = bundle.getParcelable("Key");
        String name = bundle.getString("nameKey");
        if(null != bitmap) {
            setImage(bitmap);
        } else setImageDefault();

        if(null != name) {
            setText(name);
        }
        stopProgress();
        return true;
    });

    ProgressDialog dialog = null;
    private void startProgress() {
        try {
            dialog = ProgressDialog.show(this, "",
                    "decoding...", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void stopProgress() {
        if(null != dialog) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setOpenBtn();
        setStartBtn();
        initImageView();
        setSpinner();

        wrapper = new FFmpegWrapper();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) return;
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {
            Uri iData = data.getData();
            if(null != iData) {
                String filePath = iData.getPath();
                if(null != filePath) {
                    selectedPath = makeImagePath(filePath);
                    //selectedName = makeImageName(filePath);
                    Log.d(TAG, "file path = " + selectedPath);

                    checkStartBtn();
                }
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedCodec = position;
        checkStartBtn();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // DO NOTHING
    }

    private void checkStartBtn() {
        startBtn.setEnabled(checkSelected());
    }

    private boolean checkSelected() {
        return !selectedPath.isEmpty();
    }

    private void setSpinner() {
        Spinner spinner = findViewById(R.id.videoCodecSpinner);
        if(null != spinner) {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.video_codec, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(this);
        }
    }

    private void setText(String name) {
        TextView nameView = findViewById(R.id.name_text);
        if(null != nameView) {
            nameView.setText(name);
        }
    }

    private void startDecodeMethod(String path) {
        startProgress();
        Runnable runnable = () -> action(path);
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private String makeImagePath(String returnedPath) {
        int lastIndex = returnedPath.lastIndexOf("/");
        String result = returnedPath.substring(0,lastIndex + 1);
        if(result.contains("/external_files")) {
            result = result.replace("/external_files", "");
            result = Environment.getExternalStorageDirectory().toString() + result;
        }
        return result;
    }

    /*private String makeImageName(String returnedPath) {
        int lastIndex = returnedPath.lastIndexOf("/");
        return returnedPath.substring(lastIndex + 1);
    }*/

    private String formingFilePath(String path, int number, String nameBase) {
        String result = "";
        try {
            File dir = new File(path);
            if(dir.exists() && dir.isDirectory()) {
                String nameWithNumber = String.format(Locale.ENGLISH, nameBase, number);
                result = path + nameWithNumber;
            } else {
                Toast.makeText(this, "error on readig file path", Toast.LENGTH_LONG)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private Pair<String, byte[]> openFile(String path) {
        Log.d(TAG, "-start read-");
        Pair<String, byte[]> result = null;new Pair<>("", null);
        try {
            File file = new File(path);
            if(file.exists()) {
                int size = (int) file.length();
                byte[] bytes = new byte[size];
                try {
                    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                    int read = buf.read(bytes, 0, bytes.length);
                    Log.d(TAG, "read = " + read);
                    buf.close();

                    result = new Pair<>(file.getName(), bytes);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                    result = null;
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                    result = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "-end read-");
        return result;
    }

    private void nextFrame() {
        currentImage++;
        action(selectedPath);
    }

    private void decodeSingleFrame(String path, int number, String nameBase) {
        sendMessage("декодирование...");
        Log.d(TAG, "-start decoding-");
        String fPath = formingFilePath(path, number, nameBase);
        Pair<String, byte[]> readingFrame = openFile(fPath);
        byte[][] decodingFrames = decoding(readingFrame);
        Log.d(TAG, "-end decoding-");
        if(null != decodingFrames) {
            Bitmap bitmap = getBitmap(decodingFrames[0]);
            sendBitmap(bitmap, fPath);
        } else
            sendMessage("Проблема с " + fPath);
    }

    private byte[][] decoding(Pair<String, byte[]> data) {
        byte[][] result = null;
        try{
            if(null != data && null != data.second) {
                final int videoCodec = selectedCodec;
                Pair<Integer, Integer> wh = getImageWidthHeight(videoCodec);
                String[] names = {data.first};
                byte[][] arrays = {data.second};

                result = wrapper.testByteArray3(arrays, names, wh.first, wh.second, videoCodec);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private Bitmap getBitmap(byte[] decodeFrame) {
        Bitmap bitmap = null;
        try {
            if(null != decodeFrame) {
                Pair<Integer, Integer> size = getImageWidthHeight(selectedCodec);
                bitmap = storeCameraImage(decodeFrame, size.first + 32, size.second);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private void action(String path) {
        decodeSingleFrame(path, currentImage, "frame%04d.dat");
    }

    private void sendMessage(String text) {
        Message msg = mIncomingHandler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putString("Key", text);
        msg.setData(bundle);
        mIncomingHandler.sendMessage(msg);
    }

    private void sendBitmap(Bitmap bitmap, String name) {
        Message msg = mImageHandler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putParcelable("Key", bitmap);
        bundle.putString("nameKey", name);
        msg.setData(bundle);
        mImageHandler.sendMessage(msg);
    }

    private Pair<Integer, Integer> getImageWidthHeight(int videoCodec){
        Pair<Integer, Integer> result;
        switch (videoCodec) {
            case 0:
                result = new Pair<>(640, 480);
                break;
            case 1:
                result = new Pair<>(1280, 1024);
                break;
            case 2:
                result = new Pair<>(1280, 720);
                break;
            default:
                result = new Pair<>(1280, 720);
        }
        return result;
    }


    public Bitmap storeCameraImage(byte[] imgRGB888, int width, int height) {

        Log.d("myLog", "--storeCameraImage--");
        try {
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            int[] colors = new int[width * height];
            int r, g, b;
            for (int ci = 0; ci < colors.length; ci++) {
                r = (int) (0xFF & imgRGB888[3 * ci]);
                g = (int) (0xFF & imgRGB888[3 * ci + 1]);
                b = (int) (0xFF & imgRGB888[3 * ci + 2]);
                colors[ci] = Color.rgb(r, g, b);
            }

            bitmap.setPixels(colors, 0, width, 0, 0, width, height);
            Log.d("myLog", "--storeCameraImage end--");
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("myLog", "--storeCameraImage end fail--");
        return null;
    }

    //----------------------------------------------------------------------------------------------
    private void initImageView() {
        imageView = findViewById(R.id.imageFrame);
        imageView.setOnClickListener(
                (v) -> nextFrame()
        );
    }

    private void setImage(Bitmap image) {
        if(null != imageView) {
            imageView.setImageBitmap(image);
        }
    }

    private void setImageDefault() {
        if(null != imageView) {
            imageView.setImageResource(R.drawable.ic_launcher_background);
        }
    }

    private void setStartBtn() {
        startBtn = findViewById(R.id.start_btn);
        if(null != startBtn) {
            startBtn.setOnClickListener((v) -> startBtn());
            checkStartBtn();
        }
    }

    private void startBtn() {
        currentImage = 0;
        startDecodeMethod(selectedPath);
    }

    private void setOpenBtn() {
        openBtn = findViewById(R.id.open_directory_btn);
        if(null != openBtn) {
            openBtn.setOnClickListener((v) -> onOpenDirectoryBtn());
        }
    }


    private void onOpenDirectoryBtn() {
        if(checkPermissions()) openDirectory();
        checkStartBtn();
    }

    private void openDirectory() {
        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        fileIntent.setType("resource/folder");

        try {
            startActivityForResult(fileIntent, PICKFILE_RESULT_CODE);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "No activity can handle picking a file. Showing alternatives.");
        }
    }

    //-----------------------------------------Permissions------------------------------------------
    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return false;
        } else return true;
    }

    private void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            final String message = "Storage permission is needed to show files count";
            Snackbar.make(openBtn, message, Snackbar.LENGTH_LONG)
                    .setAction("GRANT", (v) -> requestPermission())
                    .show();
        } else {
            requestPermission();

        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE == requestCode) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openDirectory();
            }
        }
    }


    //----------------------------------------------------------------------------------------------
}
