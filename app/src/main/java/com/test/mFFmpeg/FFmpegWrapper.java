package com.test.mFFmpeg;

public class FFmpegWrapper {
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("FFmpegWrapper");
    }

    public native byte[][] testByteArray3(byte[][] data, String[] names,int width, int height, int codecId);
}
